import * as vscode from 'vscode';
import * as path from 'path';
import assert from 'assert';
import { WEBVIEW_SECURITY_FINDING } from '../constants';
import { GqlSecurityFinding } from '../gitlab/security_findings/api/get_security_finding_report';
import { waitForWebview } from '../../common/utils/webviews/wait_for_webview';
import { prepareWebviewSource } from '../../common/utils/webviews/prepare_webview_source';

export type SecurityFindingWebviewPanel = vscode.WebviewPanel & {
  finding: GqlSecurityFinding;
};

export class SecurityFindingWebviewController {
  #context?: vscode.ExtensionContext;

  #panel?: SecurityFindingWebviewPanel;

  init(context: vscode.ExtensionContext) {
    this.#context = context;
  }

  #createEmptyPanel(): SecurityFindingWebviewPanel {
    assert(this.#context);

    const panel = vscode.window.createWebviewPanel(
      WEBVIEW_SECURITY_FINDING,
      '',
      vscode.ViewColumn.Active,
      {
        enableScripts: true,
        localResourceRoots: [
          vscode.Uri.file(path.join(this.#context.extensionPath, 'webviews')),
          vscode.Uri.file(path.join(this.#context.extensionPath, 'assets')),
        ],
        retainContextWhenHidden: true,
      },
    ) as SecurityFindingWebviewPanel;

    this.#panel = panel;

    panel.onDidDispose(() => {
      if (this.#panel === panel) {
        this.#panel = undefined;
      }
    });

    return panel;
  }

  async createOrUpdateWebview(
    finding: GqlSecurityFinding,
    existingPanel?: SecurityFindingWebviewPanel,
  ): Promise<SecurityFindingWebviewPanel> {
    assert(this.#context);
    const { title } = finding;
    const panel = existingPanel ?? this.#createEmptyPanel();

    panel.title = title;

    panel.webview.html = await prepareWebviewSource(
      panel.webview,
      this.#context,
      'security_finding',
    );
    await this.initPanelIfActive(panel, finding);
    return panel;
  }

  async initPanelIfActive(
    panel: vscode.WebviewPanel,
    finding: GqlSecurityFinding,
  ): Promise<null | undefined> {
    assert(this.#context);
    if (!panel.active) return;

    const waitPromise = waitForWebview(panel.webview);

    await waitPromise;
    await panel.webview.postMessage({
      type: 'findingDetails',
      finding,
    });
  }

  async open(item: GqlSecurityFinding): Promise<SecurityFindingWebviewPanel> {
    const panel = await this.createOrUpdateWebview(item, this.#panel);
    panel.reveal();
    return panel;
  }
}

export const securityFindingWebviewController = new SecurityFindingWebviewController();
